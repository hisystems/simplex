Simplex
------
###### Project ID: 16253289

A RxJS-based state management system for VueJS.

https://rxjs.dev/ https://vuejs.org/

## Dependencies
- [Docker](https://docs.docker.com/)
- [Docker Compose](https://docs.docker.com/compose/)

## Project Setup
```shell
docker login -u ${GIT_LOGIN} https://registry.gitlab.com/
```

## Project Startup
```shell
docker-compose up -d
```

## Project Shutdown
```shell
docker-compose down
```

## Project Installation
```shell
npm install @hisystems/simplex
```
