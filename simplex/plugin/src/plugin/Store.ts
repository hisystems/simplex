// IMPORT - ~/node_modules
import { Observable } from "rxjs";

// IMPORT - @/plugin
import { module } from "@/plugin/constant/proxy";
import { IOption, IReplacement, Module, TAction } from "@/plugin/Module";

// EXPORT - Interface
export interface IRoot<T, R = undefined> {
  action: Record<string, TAction<T>>;
  clone_state: () => IReplacement<T, R>;
  log: (...args: any[]) => any;
  module: {
    [Key in keyof R]: R[Key] extends IOption<infer A, infer B>
      ? IRoot<A, B>
      : IRoot<undefined>;
  };
  observables: {
    $pipeline$: Record<string, Observable<any>>;
    $state$: Observable<T>;
  };
  pipeline: Record<string, Observable<any>>;
  replace_state: (replacement: IReplacement<T, R>) => void;
  state: T;
}

// EXPORT - Class
export class Store<T, R = undefined> {
  public constructor(option: IOption<T, R>) {
    this.root = module<Module<T, R>>()(new Module<T, R>(option));
  }
  public root: Module<T, R>;
}
