// IMPORT - ~/node_modules
import { get, set } from "lodash";
import {
  assign,
  cloneDeep,
  entries,
  flow,
  forEach,
  isFunction
} from "lodash/fp";
import { BehaviorSubject, Observable, OperatorFunction } from "rxjs";
import { pipeFromArray } from "rxjs/internal/util/pipe";
import { shareReplay } from "rxjs/operators";
import Vue from "vue";
import { CombinedVueInstance } from "vue/types/vue";

// IMPORT - @/plugin
import { module as module_proxy } from "@/plugin/constant/proxy";

// EXPORT - Type
export type TAction<T> = (state: T, ...args: any[]) => Partial<T>;
export type TModule<T> = T extends IOption<infer A, infer B>
  ? IOption<A, B>
  : IOption<undefined>;
export type TPipeline<T> = OperatorFunction<T, any>[];
export type TTransform<T> = (state$: Observable<T>) => Observable<T>;

// EXPORT - Interface
export interface IOption<T, R = undefined> {
  action?: Record<string, TAction<T>> | (() => Record<string, TAction<T>>);
  module?:
    | { [Key in keyof R]: TModule<R[Key]> | (() => TModule<R[Key]>) }
    | (() => { [Key in keyof R]: TModule<R[Key]> | (() => TModule<R[Key]>) });
  pipeline?:
    | Record<string, TPipeline<T> | (() => TPipeline<T>)>
    | (() => Record<string, TPipeline<T> | (() => TPipeline<T>)>);
  replay?: number;
  state: T | (() => T);
  transform?: TTransform<T>;
}
export interface IReplacement<T, R = undefined> {
  module?: {
    [Key in keyof R]: R[Key] extends IOption<infer A, infer B>
      ? IReplacement<A, B>
      : IReplacement<undefined>;
  };
  state: T;
}

// EXPORT - Class
export class Module<T, R = undefined> {
  public constructor(option: IOption<T, R>) {
    if (option.module) {
      flow(
        entries,
        forEach(([name, module_option]) => {
          if (module_option) {
            const _module = new Module<
              typeof module_option extends IOption<infer A, {} | undefined>
                ? A
                : undefined,
              typeof module_option extends IOption<undefined, infer B>
                ? B
                : {} | undefined
            >(
              isFunction(module_option)
                ? module_option.call(module_proxy<Module<T, R>>()(this))
                : module_option
            );

            set(
              this,
              `module.${name}`,
              module_proxy<typeof _module>()(_module)
            );
          }
        })
      )(
        isFunction(option.module)
          ? option.module.call(module_proxy<Module<T, R>>()(this))
          : option.module
      );
    }

    this.state = new BehaviorSubject<T>(
      isFunction(option.state)
        ? option.state.call(module_proxy<Module<T, R>>()(this))
        : option.state
    );

    this.vm = new Vue({
      subscriptions: {
        $state$: isFunction(option.transform)
          ? option.transform
              .call(
                module_proxy<Module<T, R>>()(this),
                Observable.create((observer: any) =>
                  this.state.subscribe(observer)
                ) as Observable<T>
              )
              .pipe(shareReplay<T>(option.replay))
          : (Observable.create((observer: any) =>
              this.state.subscribe(observer)
            ).pipe(shareReplay<T>(option.replay)) as Observable<T>)
      }
    });

    if (option.pipeline) {
      set(this.vm, "$pipeline$", Vue.observable({}));

      flow(
        entries,
        forEach(([name, pipeline]) => {
          Vue.set(get(this.vm, "$pipeline$"), name, undefined);

          set(
            this.vm,
            `$observables.$pipeline$.${name}`,
            pipeFromArray(
              isFunction(pipeline)
                ? pipeline.call(module_proxy<Module<T, R>>()(this))
                : pipeline
            )(get(this.vm, "$observables.$state$"))
          );

          this.vm.$subscribeTo(
            get(this.vm, `$observables.$pipeline$.${name}`),
            result => set(this.vm, `$pipeline$.${name}`, result)
          );
        })
      )(
        isFunction(option.pipeline)
          ? option.pipeline.call(module_proxy<Module<T, R>>()(this))
          : option.pipeline
      );
    }

    if (option.action) {
      flow(
        entries,
        forEach(([name, action]) =>
          set(
            this.vm,
            `$action$.${name}`,
            (self =>
              function(...args: any[]) {
                self.state.next(
                  assign(
                    get(self.vm, "$state$"),
                    action.call(undefined, get(self.vm, "$state$"), ...args)
                  )
                );
              })(this)
          )
        )
      )(
        isFunction(option.action)
          ? option.action.call(module_proxy<Module<T, R>>()(this))
          : option.action
      );
    }

    this.clone_state = () => {
      const clone: IReplacement<T, R> = {
        state: cloneDeep(get(this.vm, "$state$"))
      };

      if (this.module) {
        flow(
          entries,
          forEach(([name, value]) =>
            set(clone, `module.${name}`, value.clone_state())
          )
        )(this.module);
      }

      return clone;
    };

    this.log = () => {
      console.log(this);
    };

    this.replace_state = (replacement: IReplacement<T, R>) => {
      if (replacement.module) {
        flow(
          entries,
          forEach(([name, value]) => {
            const _module = get(this.module, name);

            if (_module) {
              _module.replace_state(value);
            }
          })
        )(replacement.module);
      }

      this.state.next(replacement.state);
    };
  }

  public clone_state: () => IReplacement<T, R>;
  public log: (...args: any[]) => any;
  public module?: {
    [Key in keyof R]: R[Key] extends IOption<infer A, infer B>
      ? Module<A, B>
      : Module<undefined>;
  };
  public replace_state: (replacement: IReplacement<T, R>) => void;
  public state: BehaviorSubject<T>;
  public vm: CombinedVueInstance<
    Vue,
    object,
    object,
    object,
    Record<never, any>
  >;
}
