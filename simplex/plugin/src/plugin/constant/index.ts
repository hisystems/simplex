// EXPORT - Sub-Module
import * as error from "@/plugin/constant/error";
import * as proxy from "@/plugin/constant/proxy";

export { error, proxy };
