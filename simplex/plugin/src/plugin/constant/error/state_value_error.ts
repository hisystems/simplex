// EXPORT - Default
export default new Error(
  "[$simplex$]: Direct state manipulation is prohibited."
);
