// IMPORT - @/plugin
import { state_value_error } from "@/plugin/constant/error";
import { privatize } from "@/plugin/helper";

// EXPORT - Default
export default <T extends object>() => privatize<T>(state_value_error);
