// IMPORT - ~/node_modules
import { get, isObject } from "lodash/fp";

// IMPORT - @/plugin
import { state_key_error, state_value_error } from "@/plugin/constant/error";
import base from "@/plugin/constant/proxy/base";

// EXPORT - Default
export default <T extends object>() =>
  base<T>()((key: number | string | symbol) => {
    switch (key) {
      case "action":
      case "pipeline":
      case "state":
        return `vm.$${key}$`;
      case "observables":
        return `vm.$${key}`;
      case "clone_state":
      case "log":
      case "module":
      case "replace_state":
        return key;
      default:
        throw state_key_error;
    }
  })(
    (() => {
      const handler: ProxyHandler<T> = {
        construct() {
          throw state_value_error;
        },
        defineProperty() {
          throw state_value_error;
        },
        deleteProperty() {
          throw state_value_error;
        },
        get(target: T, p: number | string | symbol) {
          const result = get(p, target);

          return isObject(result)
            ? new Proxy<typeof result>(result, handler)
            : result;
        },
        getPrototypeOf() {
          throw state_value_error;
        },
        set() {
          throw state_value_error;
        },
        setPrototypeOf() {
          throw state_value_error;
        }
      };

      return handler;
    })()
  );
