// IMPORT - @/plugin
import {
  TTransform,
  default as create_private_handler
} from "@/plugin/helper/create_private_handler";

// EXPORT - Default
export default <T extends object, R extends object = {}>(error: Error) => (
  transform?: TTransform
) => (handler: ProxyHandler<R>) => (value: T) =>
  new Proxy(value, create_private_handler<T>(error)(transform)(handler));
