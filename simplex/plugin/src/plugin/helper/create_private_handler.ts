// IMPORT - ~/node_modules
import { get, isFunction, isObject } from "lodash/fp";

// EXPORT - Type
export type TTransform = (
  p: number | string | symbol
) => number | string | symbol;

// EXPORT - Default
export default <T extends object, R extends object = {}>(error: Error) => (
  transform?: TTransform
) => (handler?: ProxyHandler<R>): ProxyHandler<T> => ({
  construct() {
    throw error;
  },
  defineProperty() {
    throw error;
  },
  deleteProperty() {
    throw error;
  },
  get(target: T, p: number | string | symbol) {
    const result = get(isFunction(transform) ? transform(p) : p, target);

    return handler
      ? isObject(result)
        ? new Proxy(result, handler)
        : result
      : result;
  },
  getPrototypeOf() {
    throw error;
  },
  set() {
    throw error;
  },
  setPrototypeOf() {
    throw error;
  }
});
