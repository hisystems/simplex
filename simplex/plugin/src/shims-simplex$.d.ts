/* tslint:disable:interface-name */
// IMPORT - ~/node_modules
import Vue from "vue";

// IMPORT - @/plugin
import { Store } from "@/plugin/Store";

// DECLARATION
declare module "vue/types/options" {
  interface ComponentOptions<V extends Vue> {
    simplex$?: { root: {} } | (() => { root: {} });
  }
}

declare module "vue/types/vue" {
  interface Vue {
    $simplex$?: {};
  }
}
