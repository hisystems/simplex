// IMPORT - ~/node_modules
import { isFunction } from "lodash/fp";
import { Observable } from "rxjs";
import Vue, { VueConstructor, WatchOptions } from "vue";
import { VueDecorator, createDecorator } from "vue-class-component";
import VueRx, { Observables, WatchObservable } from "vue-rx";

// DECLARATION
declare module "vue/types/options" {
  // eslint-disable-next-line @typescript-eslint/interface-name-prefix
  interface ComponentOptions<V extends Vue> {
    __subscriptions?: Record<string, T__Subscriptions<any>>;
    _subscriptions?: Observables | TSubscriptions<any>;
    domStreams?: string[];
    observableMethods?: string[] | Record<string, string>;
  }
}

declare module "vue/types/vue" {
  // eslint-disable-next-line @typescript-eslint/interface-name-prefix
  interface Vue {
    $observables: Observables;
    $createObservableMethod(methodName: string): Observable<any>;
    $eventToObservable(event: string): Observable<{ msg: any; name: string }>;
    $fromDOMEvent(selector: string | null, event: string): Observable<Event>;
    $subscribeTo<T>(
      observable: Observable<T>,
      next: (n: T) => void,
      error?: (e: any) => void,
      complete?: () => void
    ): void;
    $watchAsObservable(
      expr: string,
      options?: WatchOptions
    ): Observable<WatchObservable<any>>;
    $watchAsObservable<T>(
      fn: (this: this) => T,
      options?: WatchOptions
    ): Observable<WatchObservable<T>>;
  }
}

// EXPORT - @/plugin
export { Store, Module, helper, constant } from "@/plugin";

// EXPORT - Default
export default (vue: VueConstructor) => {
  vue.use(VueRx);

  vue.mixin({
    beforeCreate: function simplexInit() {
      if (this.$options.simplex$) {
        this.$simplex$ = isFunction(this.$options.simplex$)
          ? this.$options.simplex$().root
          : this.$options.simplex$.root;
      } else if (this.$options.parent && this.$options.parent.$simplex$) {
        this.$simplex$ = this.$options.parent.$simplex$;
      }
    }
  });
};

// EXPORT - Types
export type TDOMStreamObservable<T extends Event> = Observable<{
  data: any;
  event: T;
}>;
export type T__Subscriptions<T extends Vue> = (
  this: T,
  subscriptions: Record<string, Observable<any>>
) => Observable<any>;
export type TSubscriptions<T extends Vue> = (this: T) => Observables;

// EXPORT - Interface
export interface IObservableMethod<T = any> extends Observable<T> {
  (...args: any[]): T;
}

// EXPORT - Function
export function DOMStream(): VueDecorator {
  return createDecorator((options, key) => {
    options.domStreams = options.domStreams ?? [];
    options.domStreams.push(key);
  });
}

export function ObservableMethod(name?: string): VueDecorator {
  return createDecorator((options, key) => {
    if (!name) {
      name = `${key}$`;
    }

    if (
      !options.observableMethods ||
      options.observableMethods instanceof Array
    ) {
      options.observableMethods = {};
    }

    if (!(options.observableMethods instanceof Array)) {
      options.observableMethods[key] = name;
    }
  });
}

export function Subscription<T, R extends Vue = Vue>(
  callback: (
    this: R,
    subscriptions: Record<string, Observable<any>>
  ) => Observable<T>
): VueDecorator {
  return createDecorator((options, key) => {
    if (!options.__subscriptions) {
      options.__subscriptions = {};

      const subscriptions: TSubscriptions<R> = function(
        this: R
      ): Record<string, Observable<any>> {
        if (options.__subscriptions) {
          const { __subscriptions } = options;

          return Object.keys(__subscriptions).reduce<
            Record<string, Observable<any>>
          >((result, name) => {
            if (name in __subscriptions) {
              return {
                ...result,
                [name]: __subscriptions[name].call(
                  this,
                  Object.keys(result).reduce(
                    (accumulator, value) => ({
                      ...accumulator,
                      [`${value}$`]: result[value]
                    }),
                    {}
                  )
                )
              };
            } else {
              return result;
            }
          }, {});
        } else {
          return {};
        }
      };

      options._subscriptions = subscriptions;
      options.subscriptions = options._subscriptions;
    }

    options.__subscriptions[key] = callback;
  });
}
