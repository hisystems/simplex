Simplex
------

A RxJS-based state management system for VueJS.

https://rxjs.dev/ https://vuejs.org/

## Usage
[Example](https://codesandbox.io/s/vue-template-7dhoo)

## Project Installation
```shell
npm install @hisystems/simplex
```
