// IMPORT - ~/node_modules
const ts_import_plugin = require("ts-import-plugin");
const uglifyjs_webpack_plugin = require("uglifyjs-webpack-plugin");

module.exports = {
  chainWebpack: config => {
    config.module
      .rule("ts")
      .use("ts-loader")
      .loader("ts-loader")
      .tap(options => {
        options.compiler = "ttypescript";
        options.happyPackMode = false;
        options.transpileOnly = false;

        return options;
      });

    if (process.env.NODE_ENV === "production") {
      config.module.rule("ts").uses.delete("cache-loader");
      config.module.rule("tsx").uses.delete("cache-loader");

      config.plugin("uglify").use(uglifyjs_webpack_plugin, [
        {
          uglifyOptions: {
            extractComments: "all",
            sourceMap: false
          }
        }
      ]);

      config.module
        .rule("ts")
        .use("ts-loader")
        .tap(args => {
          args.getCustomTransformers = () => ({
            before: [
              ts_import_plugin([
                {
                  camel2DashComponentName: false,
                  libraryDirectory: null,
                  libraryName: "lodash",
                  style: false
                },
                {
                  camel2DashComponentName: false,
                  libraryDirectory: "../_esm5/internal/operators",
                  libraryName: "rxjs/operators",
                  transformToDefaultImport: false
                },
                {
                  camel2DashComponentName: false,
                  libraryDirectory: "../_esm5/internal/observable",
                  libraryName: "rxjs",
                  transformToDefaultImport: false
                }
              ])
            ]
          });

          return args;
        });
    }
  },
  lintOnSave: true,
  outputDir: "dist/",
  parallel: process.env.NODE_ENV === "production" ? false : true,
  productionSourceMap: false,
  publicPath: "/",
  runtimeCompiler: false
};
