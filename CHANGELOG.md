# CHANGELOG

<!--- next entry here -->

## 1.0.2
2020-01-22

### Fixes

- **resource - plugin:** Fixes root module typings. (16d83fc345651cba15b025b0be960f1dbdc70e71)
- **resource - plugin:** Clean dist folders. (ebdb339b1bdc7afeb3a3891189248b035af2adb8)

## 1.0.1
2020-01-22

### Fixes

- **resource - plugin:** Fixes plugin distribution. (aef01864f5359a93cd9d36dff77b2ddc81d6154a)

## 1.0.0
2020-01-17

### Features

- **ci:** Initial Commit. (b3825290f14acf53995a7cb6d61d8742fb00131e)

### Fixes

- **project:** 1.0.1 BREAKING CHANGE:UPDATE (fc09ac6ee359305df9ecbb65812c4b70b77aa957)
- **ci - deploy:** Fixes deploy script. (7aafd5bde5a2972b2bb1b6a8285c6cf2f1476a43)
- **ci - deploy:** Fixes npm authentication. (450cafe3b2967970bfde2f686d21008459940945)
- **resource - plugin:** Fixes package homepage. (60b23b37d537e4010cf71841c4499407befa7444)

## 1.0.0-beta.1
2020-01-17

### Features

- **ci:** Initial Commit. (b3825290f14acf53995a7cb6d61d8742fb00131e)

### Fixes

- **project:** 1.0.1 BREAKING CHANGE:UPDATE (fc09ac6ee359305df9ecbb65812c4b70b77aa957)
- **ci - deploy:** Fixes deploy script. (7aafd5bde5a2972b2bb1b6a8285c6cf2f1476a43)
- **ci - deploy:** Fixes npm authentication. (450cafe3b2967970bfde2f686d21008459940945)

## 1.0.0-alpha.1
2020-01-17

### Features

- **ci:** Initial Commit. (b3825290f14acf53995a7cb6d61d8742fb00131e)

### Fixes

- **project:** 1.0.1 BREAKING CHANGE:UPDATE (fc09ac6ee359305df9ecbb65812c4b70b77aa957)
- **ci - deploy:** Fixes deploy script. (7aafd5bde5a2972b2bb1b6a8285c6cf2f1476a43)
- **ci - deploy:** Fixes npm authentication. (450cafe3b2967970bfde2f686d21008459940945)

## 1.0.0-alpha.22
2019-11-29

### Fixes

- **resource - plugin:** Fixes Finnish naming. (fe0aa4025071811e450aa173fe95423d593e0feb)

## 1.0.0-alpha.21
2019-11-27

### Features

- **resource - plugin:** Adds Finnish notation. (5f05fd4c7590ced868cea9a6630567ac8ae70ed7)

### Fixes

- **resource - plugin:** Fixes typings (5b3e347f5d3126dba715ef91236d278ce644cbaa)
- **resource - plugin:** Fixes Finnish Naming. (e47dd5ad589db29a0a15845d8e8330fd192d153a)

## 1.0.0-alpha.20
2019-11-27

### Features

- **resource - plugin:** Adds Finnish notation. (5f05fd4c7590ced868cea9a6630567ac8ae70ed7)

### Fixes

- **resource - plugin:** Fixes typings (5b3e347f5d3126dba715ef91236d278ce644cbaa)

## 1.0.0-alpha.19
2019-10-30

### Fixes

- **resource - plugin:** Fixes typings (5b3e347f5d3126dba715ef91236d278ce644cbaa)

## 1.0.0-alpha.18
2019-10-29

### Features

- **resource - app:** Adds IRoot interface. (860f30b5ed5f3438df3b09995e1247119481a7f1)

## 1.0.0-alpha.17
2019-10-25

### Fixes

- **plugin:** Refactors README.md. (0dc34e9050b9ac91a62d8bb467deb61671bcae0d)
- Add exports. (56a4e78a62950ef0411c52d0f57e9556466126a8)
- **resource - plugin:** Fixes type declarations. (e3b3525515a4038fd6bb0e01dab6440da1dcc745)

## 1.0.0-alpha.16
2019-10-23

### Fixes

- **plugin:** Refactors README.md. (0dc34e9050b9ac91a62d8bb467deb61671bcae0d)
- Add exports. (56a4e78a62950ef0411c52d0f57e9556466126a8)

## 1.0.0-alpha.15
2019-10-07

### Fixes

- **plugin:** Refactors README.md. (0dc34e9050b9ac91a62d8bb467deb61671bcae0d)

## 1.0.0-alpha.14
2019-10-07

### Fixes

- **plugin:** Fixes plugin build scripts. (c112e3a9660e366cbae57af421f0264b54af3771)

## 1.0.0-alpha.13
2019-10-03

### Fixes

- **plugin:** Refactors README.md (076dfbe22622cf2853893dbe0e6ffdad2bec71f3)

## 1.0.0-alpha.12
2019-10-03

### Fixes

- **plugin:** Adds typescript (7f6bfcf4b4c0cc3f6734c411e3a758b9d1c1e738)
- **plugin:** Fixes module system. (a4bffdb01105ff97f8e16f31ed78041975ac3b41)

## 1.0.0-alpha.11
2019-10-02

### Fixes

- **plugin:** Adds typescript (7f6bfcf4b4c0cc3f6734c411e3a758b9d1c1e738)

## 1.0.0-alpha.10
2019-07-27

### Fixes

- **simplex - plugin:** Fixes module methods. (2f4e24cb30540c5f0dd9c0d74464d4c2affe85bb)
- **simplex - plugin:** Update README.md (5f36e9f6b9a9654b10f46a05374f5c35d0d946c3)

## 1.0.0-alpha.9
2019-07-27

### Fixes

- **simplex - plugin:** Fixes module methods. (2f4e24cb30540c5f0dd9c0d74464d4c2affe85bb)

## 1.0.0-alpha.8
2019-07-27

### Fixes

- **simplex - plugin:** Fixes sub-modules. (a3277adf20a6bb0713c299cd2e7dabf53bcc3fb5)

## 1.0.0-alpha.7
2019-07-27

### Fixes

- **simplex - plugin:** Refactors install. (081bc34127e9ef22e1748db7330223e1a4477b62)

## 1.0.0-alpha.6
2019-07-23

### Fixes

- **simplex - plugin:** Update README.md (041c4aba4c378e18ff91fae8dc9572f33aea9683)

## 1.0.0-alpha.5
2019-07-23

### Fixes

- **simplex - plugin:** Fixes imports. (9247b991cd8f25bf25a48e2b33793f238882000c)
- **ci:** Fixes deploy stage. (7f30b06eca741fd062f43baaa438ae3310cb7a7c)

## 1.0.0-alpha.4
2019-07-23

### Fixes

- **ci:** Update release stage. (b6ec8f4538777ec9d475a1e1b134093c82ccf305)
- **ci:** Update stages.json (07222e6c7c4fedb2761c68573d5a9aa309def506)
- **ci:** Update .gitlab-ci.yml (c929ee278954a4216e9b7ae6b5406ec488887d1a)

## 1.0.0-alpha.3
2019-07-22

### Fixes

- **ci:** Update stages.json (675b4108f8597138198c608caa5ead8be6eac826)
- **ci:** Update stages.json (4acff071c517200ead1cbb8a6fdeedad4e27266a)

## 1.0.0-alpha.2
2019-07-22

### Fixes

- **ci:** Adds beta releases to deploy. (62be71e143986dffe1a263d25c698e227195e0c8)
- **ci:** Adds alpha releases to deploy. (47f6a4cad21baeebc644e6118b5643f79483f873)
- **docker:** Fixes docker-compose image version. (03e4c64db68852dd82b06f751ea25914225cab7c)
- **docker:** Fixes setup service. (554f9b23fd2cb7e1df5b91aa23f1f37d45fa79b9)
- **ci:** Update stages.json (69c7c759ff46f9582eaf1feeb6064462476bcb57)
- **simplex - plugin:** Corrects main in package.json. (6e472bea6a9518373ed9027d0a07920c304f89d8)
- **ci:** Corrects release job. (fa29e56bc71ea278bbc3fb888a6e1b0d8c27a273)

## 1.0.0-alpha.1
2019-07-22

### Features

- **ci:** Initial Commit. (e963f6ec0675542685578ca566459b7a6f269b04)

### Fixes

- **project:** 1.0.6 BREAKING CHANGE:UPDATE (5fc805d227609701efcce7f1fd7b46afeb7424ed)
- **project:** 1.0.7 BREAKING CHANGE:UPDATE (ea25974c4bb3fdfecaa013ab22dbbf778f43c31d)
- **project:** 1.0.8 BREAKING CHANGE:UPDATE (d62dce7e30674f002e9b5a450a1228ccaa3ddc26)
- **project:** 1.0.9 BREAKING CHANGE:UPDATE (6c33f107c2d133dca8c8e3e0021f384183f42bfc)
- **ci:** Fixes deploy stage. (a377b58ddd20b5735fd116e4654a0c6bcf895a72)
